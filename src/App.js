import React, { Component } from "react";
import "./App.css";
import Navbar from "./components/Navbar";
import Footer from "./components/Footer";
import Products from "./components/Products";
import { Route, Routes } from "react-router-dom";
import AddProduct from "./components/AddProduct";
import axios from "axios";
import Loader from "./components/Loader";
import Error from "./components/Error";
import UpdateProduct from "./components/UpdateProduct";
import { v4 } from "uuid";
import NoPageFound from "./components/NoPageFound";

class App extends Component {
    constructor(props) {
        super(props);

        this.API_STATES = {
            LOADING: "loading",
            LOADED: "loaded",
            ERROR: "error",
        };

        this.state = {
            status: this.API_STATES.LOADING,
            products: [],
            productToBeEdited: null,
        };
    }

    fetchData() {
        this.setState({
            status: this.API_STATES.LOADING,
        });
        axios
            .get("https://fakestoreapi.com/products")
            .then((res) => {
                this.setState({
                    products: res.data,
                    status: this.API_STATES.LOADED,
                });
            })
            .catch((err) => {
                this.setState({
                    status: this.API_STATES.ERROR,
                });
            });
    }

    deleteProduct = (id) => {
        let filteredItems = this.state.products.filter((product) => {
            return product.id !== id;
        });
        this.setState((prevState) => {
            return {
                ...prevState,
                products: filteredItems,
            };
        });
    };

    addProduct = (product) => {
        const productDetails = {
            id: v4(),
            title: product.title,
            description: product.description,
            category: product.category,
            image: product.imageUrl,
            price: product.price,
            rating: {
                rate: product.rating,
                count: product.count,
            },
        };
        this.setState((prevState) => {
            return {
                ...prevState,
                products: [...prevState.products, productDetails],
            };
        });
    };

    productToEdit = (id) => {
        let productToBeEdited = this.state.products.find((product) => {
            return product.id === id;
        });
        this.setState((prevState) => {
            return {
                ...prevState,
                productToBeEdited,
            };
        });
    };

    updateProduct = (updatedProduct) => {
        this.setState((prevState) => {
            return {
                ...prevState,
                products: prevState.products.map((product) => {
                    if (product.id === updatedProduct.id) {
                        return {
                            ...updatedProduct,
                            rating: {
                                rate: product.rating.rate,
                                count: product.rating.count,
                            },
                        };
                    } else {
                        return product;
                    }
                }),
            };
        });
    };

    componentDidMount() {
        this.fetchData();
    }

    render() {
        return (
            <div className="main-container">
                <Navbar />
                {this.state.status === this.API_STATES.LOADING && <Loader />}
                {this.state.status === this.API_STATES.ERROR && <Error />}
                {this.state.status === this.API_STATES.LOADED &&
                    this.state.products.length === 0 && (
                        <h1 style={{ textAlign: "center" }}>
                            No Product Found
                        </h1>
                    )}
                {this.state.status === this.API_STATES.LOADED &&
                    this.state.products.length > 0 && (
                        <Routes>
                            <Route
                                path="/"
                                element={
                                    <Products
                                        products={this.state.products}
                                        deleteProduct={this.deleteProduct}
                                        productToEdit={this.productToEdit}
                                    />
                                }
                            />
                            <Route
                                path="/add-product"
                                element={
                                    <AddProduct addProduct={this.addProduct} />
                                }
                            />
                            <Route
                                path="/update-product/:id"
                                element={
                                    this.state.productToBeEdited !== null && (
                                        <UpdateProduct
                                            product={
                                                this.state.productToBeEdited
                                            }
                                            updateProduct={this.updateProduct}
                                        />
                                    )
                                }
                            />
                            <Route path="*" element={<NoPageFound/>}/>
                        </Routes>
                    )}
                <Footer />
            </div>
        );
    }
}

export default App;
