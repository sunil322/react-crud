import React, { Component } from "react";
import ProductDetailsForm from "./ProductDetailsForm";

class AddProduct extends Component {
    
    addProduct = (product) => {
        this.props.addProduct(product);
    };

    render() {
        return <ProductDetailsForm addProduct={this.addProduct} header={"Add Product Information"} buttonText={"Add Product"}/>;
    }
}

export default AddProduct;
