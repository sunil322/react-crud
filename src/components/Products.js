import React, { Component } from "react";
import "./Products.css";
import Product from "./Product";

class Products extends Component {

    deleteProduct = (id) =>{
        this.props.deleteProduct(id);
    }
    productToEdit = (id) =>{
        this.props.productToEdit(id);
    }

    render() {
        return (
            <div className="products-container">
                {this.props.products.map((product) => {
                    return (
                        <Product
                            key={product.id}
                            product={product}
                            deleteProduct={this.deleteProduct}
                            productToEdit = {this.productToEdit}
                        />
                    );
                })}
            </div>
        );
    }
}

export default Products;
