import React, { Component } from "react";
import "./Footer.css";

class Footer extends Component {
    render() {
        return (
            <footer>
                <p>&copy; 2023 FakeStore.com</p>
            </footer>
        );
    }
}

export default Footer;
