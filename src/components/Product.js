import React, { Component } from "react";
import "./Products.css";
import { Link } from "react-router-dom";

class Product extends Component {
    constructor(props) {
        super(props);
        this.state = {
            clickedRemoveButton: false,
        };
    }

    handleCancelButton = () =>{
        this.setState({
            clickedRemoveButton: false
       });
    }
    handleConfirmButton = () =>{
        this.props.deleteProduct(this.props.product.id);
        this.setState({
            clickedRemoveButton: false
       });
    }

    deleteProduct = (id) => {
        this.setState({
            clickedRemoveButton: true,
        });
    };
    productToEdit = (id) => {
        this.props.productToEdit(id);
    };

    render() {
        const { id, title, category, description, image, price, rating } =
            this.props.product;
        return (
            <div className="product">
                <div className="product-img">
                    <img src={image} alt="product" />
                </div>
                <div className="product-info">
                    <p className="category">{category}</p>
                    <p className="title">{title}</p>
                    <p className="description">{description}</p>
                    <p className="price">
                        Price : <span>${price}</span>
                    </p>
                    <p className="rating">
                        <span>
                            <i className="fa-solid fa-star"></i> {rating.rate} (
                            {rating.count})
                        </span>
                    </p>
                </div>
                <div className="button-div">
                    <Link to={`/update-product/${id}`}>
                        <button
                            className="update-btn"
                            onClick={() => this.productToEdit(id)}
                        >
                            <i className="fa-solid fa-pen-to-square"></i>
                            <span>UPDATE</span>
                        </button>
                    </Link>
                    {this.state.clickedRemoveButton ? (
                        <div className="button-toggle-div">
                            <button className="confirm-btn" onClick={this.handleConfirmButton}>Confirm</button>
                            <button className="cancel-btn" onClick={this.handleCancelButton}>Cancel</button>
                        </div>
                    ) : (
                        <button
                            className="delete-btn"
                            onClick={() => this.deleteProduct(id)}
                        >
                            <i className="fa-sharp fa-solid fa-trash"></i>
                            <span>DELETE</span>
                        </button>
                    )}
                </div>
            </div>
        );
    }
}

export default Product;
