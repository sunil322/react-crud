import React, { Component } from "react";
import "./Navbar.css";
import logo from "../images/logo.png";
import { Link,NavLink } from "react-router-dom";

class Navbar extends Component {
    render() {
        return (
            <>
                <nav>
                    <div className="nav-logo">
                        <Link to="/">
                            <img src={logo} alt="logo" />
                        </Link>
                    </div>
                    <div className="nav-menu">
                        <ul>
                            <li><NavLink to="/">Home</NavLink></li>
                            <li><NavLink to="/add-product">Add Product</NavLink></li>
                        </ul>
                    </div>
                </nav>
            </>
        );
    }
}

export default Navbar;
